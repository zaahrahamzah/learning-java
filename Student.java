// TUTO 82 - Inheritance

public class Student extends User implements Talk {
	public String major;
	
	//TUTO 90 - Creating default constructor 
	
	public Student() {
		super();
		System.out.println("A student is created");
	}
	
	public Student(String fn, String ln) {
		super(fn, ln);
	}

	@Override
	public void sayHello() {
		System.out.println("Implemented at student");
		
	}
	

}
