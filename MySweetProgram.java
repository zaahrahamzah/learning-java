import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MySweetProgram {
	
	int count = 0;
	public static void main(String[] args) {

		// TUTO 1 - Taking Input  //

		/**String name;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("What's your name");
		String name = scanner.nextLine();
		System.out.println("Hello! wo shi " + name);	
		
		int x = scanner.nextInt(16);		
		System.out.println(x);**/


		// TUTO 2 - String //  	
		
		/**String string = new String ("This is the hard way to store string ");
		String easy = " This is the easier version to store string";		
		System.out.println(string + easy);**/


		// TUTO 3 - Primitive and object type // 
		
		/**int x = 5; //primitive
		Integer y = 5; //object (autoboxed - conversion to object)
		System.out.println(x+y);**/
		

		// TUTO 4 - String methods // 
		
		/**String x = "Pinggang ku sakit separuh mati";
		System.out.println(x.indexOf("ku"));**/
		

		// TUTO 5 - OOP introduction //
		
		/**User user = new User();
		user.firstName = "Syer";
		user.lastName = "Cantek";
		System.out.println(user.getFullName());**/
		

		// TUTO 6 - String Comparison // 
		
		/**Scanner scanner = new Scanner(System.in);
		String guess = scanner.nextLine();
		String password = "password";
		System.out.println(password.equals(guess));
		System.out.println(password == guess);
		
		String a = "nano";
		String b = "nano";
		System.out.println(VM.current().addressOf(obj)));**/
		

		// TUTO 7 - Switch Statements // 
		
		/**switch (name) {
			case "Timah":
				System.out.println("ELO ELO YOU SO PRETTY OKAY BYE");
				break;
			case "Farah":
				System.out.println("ELO ELO YOU DAMN SMART");
				break;
			case "Syer":
				System.out.println("ELO CERRY PIEEE");
				break;
			default:
				System.out.println("Try again later boo salah namabi");
		}**/
		
		
		// TUTO 8 - if else statements //
		
		/**if (name.equals("Caleb") || name.equals("Clare"))  { //I tried using (name == "Caleb" || name == 			          												//Clare") but doesnt work cause it's object 
																//types and compare the memory location 
			System.out.println("Stash Away hehe");
		} 
		else {
			System.out.println("Try again some other time dont forget to wear mask tho bye");
		}**/		
		
		
		// TUTO 9 - Ternary Operator // 
		
		/**boolean welcome = name.equals("Timah") ? true : false;
		
		if (welcome) {
			System.out.println("Elo Timah");
		}
		else {
			System.out.println("Elo not Timah");
		}**/
		

		// TUTO 10 - While Loop // 
		
		/**System.out.println("Guess the password");
		String password = "let me in";
		
		Scanner scanner = new Scanner(System.in);
		String guess = scanner.nextLine();
		
		while (!password.equals(guess)) {
			System.out.println("Guess the password");
			guess = scanner.nextLine();
		}**/
		
		
		// TUTO 11 - Do While Loop //
		
		/**String password = "let me in";		
		Scanner scanner = new Scanner(System.in);
		String guess;
		
		do {
			System.out.println("Guess the password");
			guess = scanner.nextLine();
			
		} while (!password.equals(guess));
		
		System.out.println("you guess corerctly");**/
		

		// TUTO 12 - For Loop //
				
		/**for (int i = 9; i > 0; i--) {
			System.out.println(i);
		}**/
		

		// TUTO 13 - Nested For Loop //
		
		/**for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}**/
		

		// TUTO 14 - Array //
		
		/**int[] grades = {0,1,2,3,4,5,6,7,8,9};
		
		System.out.println(grades[1]);
		try {
			System.out.println(grades[10]);
		}
		catch (Exception e) {
			System.out.println(e);
		}**/
		

		// Tuto36 - Array Values from input with for loop //
		
		/**Scanner input = new Scanner(System.in);
		
		int size = input.nextInt();
		int[] grade = new int[size];
		
		for (int i = 0; i < size; i++) {
			int element = input.nextInt();
			grade[i] = element;
		}
		
		for (int i = 0; i < size; i++) {
			System.out.println(grade[i]);
		}**/
		
		// Tuto38 - sorting array // 
		
		/**int[] sot = {1,5,9};
		
		for (int i = 0; i < 2; i++) {
			for (int j = i+1; j < 2; j++) {
				if (sot[i] < sot[j+1]) {
					int temp = sot[i];
					sot[i] = sot[j+1];
					sot[j+1] = temp;				
				}
			}
		}
		
		System.out.println(Arrays.toString(sot));**/
		

		// Tuto 41,42 - working with 2D Arrays // 
		
		/**	int[][] grades = {
					{1,5,3},
					{8,4,2,4,3,5,3},
					{4,6,2}
			};
			
			for (int row = 0; row < grades.length; row++) {
				for(int col = 0; col < grades[row].length; col++ ) {
					System.out.print(grades[row][col] + " ");
				}
				System.out.println("");
			} **/


		// TUTO 43 - Intro to Array List //		
		
		/** ArrayList  
		 * - can dynamically resize
		 * - ArrayList<Integer> grades
		 * = new ArrayList<Integer>();
		 * 
		 * to push
		 * - grades.add(i)
		 * 
		 * to add element 
		 * - grades.get(index);
		 * 
		 * to update elem 
		 * - grades.set(0,10);
		 *  
		 *  to ask for the size
		 * - grades.size();
		 *  
		 *  **/
		

		// Tuto 44 - List Interface and ArrayList Implementation //  
		
		/**interface cth: linked list, arraylist**/
		
		/**	List<Integer> grades = new ArrayList<Integer>(); 
			grades.add(5);
			grades.add(10);
			grades.add(1, 7); // add(index, element)
			grades.clear();
			
			System.out.println(grades.get(0));
			System.out.println(grades.contains(10));
			System.out.println(grades.isEmpty()) ; **/
		

		// TUTO 45 - to convert array to list //
		
		/**List<Integer> grades = Arrays.asList(1,2,3,4,5);
		 
		System.out.println(Arrays.toString(grades.toArray()));
		
		int[] grades2 = {5,6,7,8};
		
		System.out.println(Arrays.toString(grades2));**/ 
		

		// TUTO 47 - For Loops with list & how to modify each element //
		
		/**for(int i = 0; i < grades.size(); i++) {
			grades.set(i, grades.get(i)*2);
			System.out.print(grades.get(i) + " ");
		}**/
		

		// TUTO 48 FOR EACH LOOP IN JAVA //
		
		/**for (int grade: grades) {
			System.out.print(grade + " ");
		}**/
		
		
		// TUTO 49 NESTED FOR EACH LOOP //
		
		/**List <List<Integer>> allGrades = new ArrayList <List<Integer>>();
		allGrades.add(Arrays.asList(1,2,3));
		allGrades.add(Arrays.asList(4,5,6));
		allGrades.add(Arrays.asList(7,8,9));
		
		for (List<Integer> grades: allGrades) {
			for (int grade : grades) {
				System.out.print(grade + " ");
			}
			System.out.println();
		}**/
		
		// TUTO 50 convert List to Array
		
		List<Integer> allGrades = Arrays.asList(1,7,3,9,0);
		/**int[] grades = new int[allGrades.size()];
		
		for (int i = 0 ; i < allGrades.size() ; i++) {
			
			grades[i] = allGrades.get(i);
			
		}
		
		System.out.println(Arrays.toString(grades));**/
		

		// TUTO 51 - REVERSE LIST SORT // 		
		
		/**Collections.sort(allGrades);
		Collections.reverse(allGrades);

		for (int grade : allGrades) {
			System.out.print(grade + " ");
		}**/


		// Tuto 52-56 INTRO TO OOP, WHY WE NEED OOP // 
		
		/** 
		 * Object - structures to represent something 
		 * 
		 * Class vs Object  
		 * class is a structure/blueprint/skeleton 
		 * object is something being made from class (instances)
		 * e.g class = human, object = timah 
		 * class = custom type
		 * object is made/instantiate by making a variable from class
		 * 	 
		 * FIELD - a variable that we can assgn a value - inside of a class
		 * 
		 * PUBLIC VS PRIVATE [Access Modifiers]
		 * public - open access level
		 * private - the thing that can be acces using public method
		 * ~basically this is how you make your code more 'structured'~
		 * 
		 * METHOD - getter setter 
		 * 
		 * **/
		

		// TUTO 57,58 - BASICS OF CREATING OBJECT AND CLASS // 
		
		/**User you = new User();
		you.setName("Fatimah 1", "hamzah");	
		
		User me = new User();
		me.setName("Zahra 2", "Hamzah");**/
		

		// TUTO 59 - Method //
		
		/**user.output(5);**/
		

		// TUTO 60 Return Statement // 
		
		/**String message = user.output(); 
		System.out.println(message);**/
		

		// TUTO 61  Encapsulation //

		/**
		 * hide inner working, and work with the variable without 
		 * changing much of the var structure
		 *  
		 * **/
		
		// TUTO 66,67 Array List of Custom Type, create custom type in loop //
		
		/**String[] firstName = {"Nurul", "Siti", "Nur"};
		String[] lastName = {"Sabirah", "Syuhada", "Syahirah"};**/
		
		/**List <User> userList = new ArrayList<User>();**/
		
		/**for (int i = 0 ; i < firstName.length ; i++) {
			User u = new User();
			u.setName(firstName[i], lastName[i]);
			userList.add(u);
		}**/
		
		/**for (User user : userList) {
			System.out.println(user.getFullName());
		}**/
		

		// TUTO 68 //
		
		/**m.printUser(userList.get(1)); **/
		
		// TUTO 69 - Intro to Static Method //
		
		/**
		 * It's attached to a class rather than the object of the class
		 * 
		 * e.g: 
		 * 	instance method will be like: 
		 * 		User u = new user;
		 * 		u.talk();
		 * 
		 *  static method will be like:
		 *  	User.findInList();
		 * 	
		 * **/
		

		// TUTO 70 - how to create static method //
		
		/**
		 * Basically just putting the word static in the method 
		 * and put it dekat the User class file
		 * 
		 * **/
		
		/**User.printUser(userList.get(2));**/
		

		// TUTO 71 - Method to take an ArrayList of custom type //
		
		/**User user1 = new User();
		user1.setFirstName("Adib");
		user1.setLastName("Zahed");
		
		User user2 = new User();
		user2.setFirstName("Not");
		user2.setLastName("Me");
		
		userList.add(user1);
		userList.add(user2);**/
		
		//User.printUsers(userList);
		
		// TUTO 72 - Intro to method overloading and Optional parameters
		
		/**
		 * when two methods have the same name but different parameters 
		 *  
		 * **/
		
		/**System.out.println(User.searchList(userList, "abu", "Zahed"));**/
		
		// TUTO 73 - Method Overriding
		
		/**
		 * When a derived class have a specific method. 
		 * 
		 * e.g: 
		 * 	Student work() method override User work() method
		 * 	- meaning that the student version is more specific whereas the User version is more generalized. 
		 * 
		 * **/
		
		//System.out.println(user1.toString());
		

		// TUTO 77 - OVERRIDE EQUALS //
		
		//System.out.println(user1.hashCode() == user2.hashCode());
		

		// TUTO 78 - OVERLOAD SEARCH METHOD TO TAKE IN USER AS PARAMETER //
		
		//System.out.println(User.searchList(userList, user2));


		// TUTO 79 - RETURNING CUSTOMS OBJECTS //
		
		/**List<User> userList = new ArrayList<User>();
		
		User user1 = new User();
		user1.setFirstName("Fatimah");
		user1.setLastName("Zahra");
		
		User user2 = new User();
		user2.setFirstName("Khadijah");
		user2.setLastName("Abu");
		
		userList.add(user2);
		userList.add(user1);
		
		User found = User.findUser(userList, user2);
		
		System.out.println(found);**/


				// TUTO 81 - Intro to Inheritance
		
		/**
		 * OOP - Encapsulation, Inheritance, 
		 * Object ->  User (custom type) 
		 * 
		 * **/

		
		// TUTO 82 - Inheritance
		
		/**Student s = new Student();
		s.firstName = "Timah";
		s.lastName = "Jompot";
		s.major = "Sleepy design";
		
		System.out.println(s.major);**/
		

		// TUTO 83 - Virtual in Java 
		
		/**
		 * virtual function -  a fx that is a member fx of a base class
		 * 						that is override by a derived class
		 * java do that automatically, hence we dont need to explicitly state the virtual
		 * 
		 * diff btwn abstract methods and  virtual
		 * - virtual methods provide the opt for the subclass to override 
		 * - abs. method doestnt, at the base class, the method isnt define, 
		 *   hence subclass needs to override it .
		 * 
		 * **/
		

		// TUTO 84 - creating a method in User class and Overriding it. 
		 /**s.sayHello();**/
		 

		 // TUTO 85 - abstract class
		 
		 /**
		  * a class that cannot be instatiated.
		  * - e.g Animal, doesnt exist just animal, but exist a pecific animal like
		  * 	dog, cat. hence why its useful 
		  * - when want to use the class, we can still use the things inside the body of the
		  *   class jutst cant instatiated directly - need create subclass
		  * **/
		 


		 // TUTO 86 - abstract method 
		 
		 /**
		  * need to define the method at the subclass level
		  * the abstract method cannot have a body at the base class level
		  * 
		  * **/
		 

		 // TUTO 87 - Polymorphism //
		 
		 /**
		  * emmm basically we can call the method depends on the context
		  * e.g: if we see 'person' and we dont know them, we prolly wont say hi
		  * 		but if the 'person' is our 'friend' , then we prolly will say hi
		  * 		and ask hows it going bla bla
		  * **/
		 

		 // TUTO 88 - Polymorphism in practice //
		 
		 /**Teacher t = new Teacher();
		 
		 List<User> userList = new ArrayList();
		 userList.add(t);
		 userList.add(s);
		 
		 for (User u : userList) {
			 u.sayHello(); // the output will be, each user will respond to their 
			 					// specific person (teacher, student)
		 }**/
		 

		 // TUTO 89 - Intro to Constructors //
		 
		 /**
		  * - being called when we instatiated an object 
		  * - like a method but doesnt have any return type
		  * - can make custom or default
		  * - xda kaitan but *final method cannot be overridden 
		  * 
		  * **/
		 
		 // TUTO 90 - Creating default constructor //
		 
		 /**Student s = new Student();**/
		

		 // TUTO 91 - Creating a custom constructor //  
		
		 /**Student s1 = new Student("Timah", "Jompot");**/
		 

		 // TUTO 92 - Invoke Parent class methods with Super keyword //
		 
		 /**Using it in the derived class of the method
		 
		 s.sayHello();**/
		 

		 // TUTO 93 - Read Only (private) fields assigned with constructors // 
		 
		 s1.sayHello();
		 

		 // TUTO 94 - Intro to Interfaces //
		 
		 /**
		  * a set of behavior/method that could be re used by objects
		  * Example: A cat (obj) can walk, talk (methods) this walk talk methods 
		  * 			can also be used by dogs, other animals/objects etc. Hence,
		  * 			this set of methods are call interface. 
		  * 
		  * if inheriting a class we used - extends
		  * then, using interface we used - implements 
		  * 
		  * **/
		 

		 // TUTO 95 - Creating an interface of functionality //
		 
		 /**List<User> thingsThatTalk = new ArrayList();
		 thingsThatTalk.add(s1);**/
		 

		 // TUTO 96,97 - Final classes and methods //
		 
		 /**
		  * Final methods - so that no subclasses can override the method
		  * Final classes - no class can extends from the final class
		  * 
		  * **/
		 

		 // TUTO 98 - Intro to enumeration //
		 
		 /**
		  * allow us to have a list of possible value for a variable 
		  **/
		 
		 /**s1.theStatus = s1.theStatus.probation;
		 System.out.println(s1.theStatus);**/

		 
		 // TUTO 99 - Enum in switch //
		 
		 /**switch (s1.theStatus) {
		 case active:
			 System.out.println("Your current status is active");
			 break;
		 case inactive:
			 System.out.println("Your current status is inactive");
			 break;
		 case probation:
			 System.out.println("Your current status is probation");
			 break;
		 }**/
		 

		 // TUTO 100 - Conclusion // 
		 
		 /**
		  * more topics to discover:-
		  * 	- generic programming
		  * 	- data structures
		  * 	- database reading (i/o files)
		  * 	- exception and error handling
		  * 	- debugging 
		  * 
		  * **/
	
	}
	
	// TUTO 68 - Taking custom types as argument //
	
	/**public void printUser (User u) {
		System.out.println("tuto 68 " + u.getFullName());
	}**/

	/**User me = new User();
		me.setFirstName("Timoh");
		me.setLastName("Meh");
		
		User you = new User();
		you.setFirstName("Adib");
		you.setLastName("Kambing");
		
		List<User> userList = new ArrayList<User>();
		userList.add(me);
		userList.add(you);
				
		System.out.println(you.getFullName());**/

}
