import java.util.List;

public class User {
	private String firstName;
	private String lastName;
	
	//TUTO 63 Create getter 
	public String getFirstName() {
		return firstName;
		
	}
	
	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}
	
	//create setter	
	public void setFirstName(String fn) {
		firstName = fn;
	}
	
	public String getLastName() {
		return lastName;
		
	}
	
	//create setter	
	public void setLastName(String ln) {
		lastName = ln;
	}
		
	// TUTO 70 - How to create static method 
	public static void printUser (User u) {
		System.out.println("tuto 70 " + u.getFullName());
	}
	
	// TUTO 71
	public static void printUsers(List<User> users ) {
		for (User usr : users )
		System.out.println(usr.getFullName());
	}
	
	//TUTO 74 - Search List 
	
	/**public static int searchList (List<User> user, String fname, String lname) {
		return searchList (user, fname + " " + lname);
	}**/
	
	public static int searchList (List<User> userList, int hashCode) {
		for (int i = 0; i < userList.size(); i++) {
			if (userList.get(i).hashCode() == hashCode) {
				return i; 
			}
		}
		return -1;
	}

	public static int searchList (List<User> userList, User u) {
		return searchList(userList, u.hashCode());
	}
	
	@Override
	public String toString() {
		return "User [getFullName()=" + getFullName() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
	
	// TUTO 79 - Returning Custom Objects
	
	public static User findUser(List<User> userList, User u) {
		for (User user : userList) {
			if (user.equals(u)) {
				return user;
			}
		}
		return null;
	}

	// TUTO 98 - Intro to enum
	
	public enum status {active, inactive, probation}; // it's like declaring a type 
	public status theStatus;	
	
	//public abstract void sayHello();
	
	public User () {
		System.out.println("User constructor");
	}
	
	public User (String fn, String ln) {
		firstName = fn;
		lastName = ln;
	}
	
	
	
}
